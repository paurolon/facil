<?php include( 'inc_head.php'); $weblocation=1; $_SESSION[ 'productos_comparar']=array(); //unset($_SESSION); ?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8" />
    <title>Facilandia Beta</title>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="stylesheet" type="text/css" href="fonts/ico/flaticon.css">

    <link href="css/normalize.min.css" rel="stylesheet" type="text/css" />



    <link href="css/estilos.css" rel="stylesheet" type="text/css" />
    <?php include_once( 'inc_header.php'); ?>
    <script type="text/javascript" src="inc/swfobject_modified.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        sliderHomeSetup();
        cargarBannerHome_A();
        cargarBannerHome_B();
        $(document).on('webload', function() {
            alert();


        });
    });
    </script>

    <style type="text/css">
    </style>
</head>


<body>
    <div id="global_wrap">
        <div id="header_wrap">
            <div id="header_wrap_top">
                <div id="header_wrap_top_cont">
                    <div id="header_wrap_top_left">
                        <a href="index.php">
                            <img src="imagenes/src/logo.png" height="117" border='0' alt="facilandia" />
                        </a>
                    </div>
                    <div id="header_wrap_top_right">
                        <div id="header_wrap_top_log">
                            <?php if(isset($_SESSION[ 'validuser_facil'])) { ?>
                            <div class="header_wrap_top_log_elem2"><a href="index.php?logout=true">CERRAR SESION </a>
                            </div>
                            <div class="header_wrap_top_log_elem2">Bienvenido
                                <?php echo $_SESSION['validuser_facil_nombre']. " ".$_SESSION['validuser_facil_apellido']; ?>&nbsp&nbsp&nbsp&nbsp&nbsp
                            </div>
                            <?php } else { ?>

                            <form method="post" action="index.php" autocomplete="off">


                                <div class="header_wrap_top_log_elem">
                                    <input type="hidden" name="loggin" value="true" />
                                    <input type="image" src="imagenes/src/mi_cuenta_ir.png" height="25" />
                                </div>
                                <div class="header_wrap_top_log_elem">
                                    <input type="password" class="header_wrap_top_log_inp" placeholder="password" name="password" style="margin-left:10px" />
                                </div>

                                <div class="header_wrap_top_log_elem">
                                    <input type="text" class="header_wrap_top_log_inp" placeholder="email@email.com" name="usuario" autocomplete="off" />
                                </div>


                                <div class="header_wrap_top_log_elem">
                                    <div id="header_wrap_top_log_tit">MI CUENTA</div>
                                </div>


                                <div id="header_wrap_top_log_elem_regist"> <a href="registro.php"> Registrarse</a>  <a href=""> ¿Olvidaste tu contraseña?</a> 
                                </div>
                                <div class="spacer"></div>
                            </form>
                            <?php } ?>
                        </div>
                        <div id="header_wrap_top_log2">
                            <div class="header_wrap_top_log_elem">
                                <img src="imagenes/src/buscador_icono.png" height="25" onclick="buscarprod()" />
                            </div>
                            <div class="header_wrap_top_log_elem">
                                <input type="search" class="header_wrap_top_log_inp2" id="searchinput" placeholder="Ingresa marca, modelo ó código" onkeypress="checkKey(event);" />
                            </div>

                        </div>
                    </div>
                    <div class="spacer"></div>
                </div>
                <div class="spacer"></div>
                <span class="hmenuit_home <?php if($weblocation==1) echo " hmenuit_home_active ";?>"></span>
            </div>
            <div id="header_menu_box">
                <div id="header_menu_box_content">
                    <ul>


                        <li class="header_menu_principal ">
                            <a href="index.php" class="header_menu_item header_menu_principal_a  <?php if($weblocation==1) echo " header_menu_item_active ";?>">
                                <span>INICIO</span>
                                <i class="flaticon-dwelling1"></i>
                            </a>
                        </li>


                        <li class="header_menu_item header_menu_principal <?php if($weblocation==2) echo " header_menu_item_active ";?> ">
                            <a href="categoria.php" class="header_menu_item header_menu_principal_a">
                                <span>PRODUCTOS</span>
                                <i class="flaticon-list30"></i>
                            </a>
                            <ul id="MenucategoriasProductos">

                            </ul>
                        </li>


                        <li class="header_menu_principal">
                            <a href="comparar.php" class="header_menu_item header_menu_principal_a <?php if($weblocation==3) echo " header_menu_item_active ";?> ">
                                <span>COMPARAR PRODUCTOS</span><i class="flaticon-star138"></i>
                            </a>
                        </li>


                        <li class="header_menu_principal">
                            <a href="bodas-home.php" class="header_menu_item header_menu_principal_a <?php if($weblocation==4) echo " header_menu_item_active ";?> ">
                                <span>LISTA DE BODAS</span>
                                <i class="flaticon-diamond22"></i>

                            </a>
                        </li>


                        <li class="header_menu_principal">
                            <a href="sucursales.php" class="header_menu_item header_menu_principal_a <?php if($weblocation==5) echo " header_menu_item_active ";?> ">
                                <span>SUCURSALES</span><i class="flaticon-magnifier13"></i>

                            </a>
                        </li>


                        <li class="header_menu_principal">
                            <a href="deseos.php" class="header_menu_item header_menu_principal_a <?php if($weblocation==6) echo " header_menu_item_active ";?> ">
                                <span>LISTA DE DESEOS</span><i class="flaticon-shopping2"></i>
                            </a>
                        </li>


                    </ul>
                </div>
            </div>
        </div>
        <div id="content_wrap">
            <div id="home_banners_boxes">
                <div id="home_top_banner">

                    <!--<img src="imagenes/src/bannerestatico.jpg" width="950" height="80" /> -->
                    <?php $dbbanner=new BannersWeb; $dbbanner->banner_fijo_web(950,80); ?>
                </div>
                <div id="home_mid_banner">
                    <div id="home_mid_banner_cont">

                    </div>
                </div>
            </div>
            <div id="home_divisor">Porque nos encanta hacerte la vida más fácil, te ofrecemos los diferentes servicios:</div>
            <div id="index_banner_servicios">
                <div id="index_banner_servicios_cont">
                    <a href="deseos.php#llamada">
                        <img src="imagenes/src/banner_tellamamos_en_2_minutos.png" />
                    </a>
                    <a href="bodas-home.php">
                        <img src="imagenes/src/banner_lista_de_bodas.png" />
                    </a>
                    <a href="/mibew/client.php?locale=es&amp;style=original" target="_blank" onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &amp;&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open(&#039;/web/mibew/client.php?locale=es&amp;style=original&amp;url=&#039;+escape(document.location.href)+&#039;&amp;referrer=&#039;+escape(document.referrer), 'mibew', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=640,height=480,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;">
                    
                      






                        <img src="imagenes/src/banner_atencion_en_linea.png"  border="0" />
                    </a>
                </div>
            </div>

<!-- <a onclick="popupwindow();" > -->

           <script type="text/javascript">
            function popupwindow() {
                //alert();
                url = 'contacto.php';
                title = 'Facilandia';
                w = 650;
                h = 700;
                var left = (screen.width / 2) - (w / 2);
                var top = (screen.height / 2) - (h / 2);
                return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
            }
            </script> 



            <div id="index_valorados">
                <div id="index_valorados_titulo_box">DESTACADOS</div>
                <div id="index_valorados_productos">
                    <div id="index_valorados_productos_slider_next"></div>
                    <div id="index_valorados_productos_sl2">
                        <div id="index_valorados_productos_sl">


                        </div>
                    </div>
                    <div id="index_valorados_productos_sl_slider_prev"></div>
                    <div class="spacer"></div>
                </div>
            </div>
            <div class="spacer"></div>
            <br />
            <br />
            <br />
            <div class="spacer"></div>
            <div id="index_categorias_box">
                <div id="index_categoria_titulo">CATEGORIAS DESTACADAS</div>
                <div id="index_categorias_box_cont">


                </div>
            </div>
            <div class="spacer"></div>
            <a id="index_ruleta">
                <object type="application/x-shockwave-flash" width="950" height="80" data="imagenes/src/bannerssss.swf">
                    <param name="movie" value="imagenes/src/bannerssss.swf" />
                    <param name="menu" value="false" />
                    <param name="quality" value="high" />
                </object>
            </a>
            <div id="footer_box">
                <div id="footer_box_cont">
                    <div class="footer_element">
                        <div class="footer_element_tit">FACILANDIA</div>
                        <div id="footer_menu">
                            <ul>
                                <li><a href="">Sucursales &nbsp;&nbsp;<img src="imagenes/src/flecha_derecha_gris.png" height="10" /></a>
                                </li>
                                <li><a href="nosotros.php">Quienes Somos &nbsp;&nbsp;<img src="imagenes/src/flecha_derecha_gris.png" height="10" /></a>
                                </li>
                                <li><a href="trabaja-con-nosotros.php">Envianos tu CV&nbsp;&nbsp; <img src="imagenes/src/flecha_derecha_gris.png" height="10" /></a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="footer_element">
                        <div class="footer_element_tit">REDES SOCIALES</div>
                        <div id="footer_redes">
                            <a href="https://twitter.com/facilandiapy" target="_blank">
                                <div class="footer_redes_item">
                                    <img src="imagenes/src/twitter.png" height="20" />@facilandia</div>
                            </a>
                            <a href="https://www.facebook.com/facilandia" target="_blank">
                                <div class="footer_redes_item">
                                    <img src="imagenes/src/facebook.png" height="20" />Facilandia</div>
                            </a>
                            <a href="http://www.youtube.com/user/Facilandia/videos" target="_blank">
                                <div class="footer_redes_item">
                                    <img src="imagenes/src/youtube.png" height="20" />Facilandia</div>
                            </a>
                        </div>
                    </div>
                    <div class="footer_element">
                        <div class="footer_element_tit">ATENCION EN LINEA</div>
                        <div id="footer_element_txt">Chatea con un operador y hacenos tus consultas en línea!
                            <br />
                            <br />
                            <a href="/mibew/client.php?locale=es&amp;style=original" target="_blank" onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &amp;&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open(&#039;/web/mibew/client.php?locale=es&amp;style=original&amp;url=&#039;+escape(document.location.href)+&#039;&amp;referrer=&#039;+escape(document.referrer), 'mibew', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=640,height=480,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;" id="footer_atnlin_btn"> CLICK AQUÍ</a> 
                        </div>
                    </div>
                    <div class="footer_element" style="border: none">
                        <div class="footer_element_tit">CONTACTO</div>
                        <div id="footer_element_txt">Visitanos en cualquiera de nuestras sucursales o llamanos al:
                            <br />
                            <br />
                            <span id="footer_atnlin_btn2">
                                <img src="imagenes/src/icone-telefone-contato.png" width="38" height="38" />238 14 44</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
